# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 16:37:20 2018

@author: Michael Thomas Bogumil
"""


import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext


win = tk.Tk()

win.title("MiLAMP Finder")

win.resizable(False, False)

name = tk.StringVar()
name_entered = ttk.Entry(win, width=12, textvariable=name)
name_entered.grid(column=0,row=1)

scrol_w = 30

scrol_h = 3

scr = scrolledtext.ScrolledText(win, width=scrol_w, height = scrol_h, wrap=tk.WORD)
scr.grid(column = 0, columnspan = 3)

name_entered.focus()


a_label = ttk.Label(win, text="A Label")
a_label.grid(column = 0, row = 0)


def click_me():
    action.configure(text=name_entered.get())
    a_label.configure(foreground='red')
    a_label.configure(text='A Red Label')


action = ttk.Button(win, text="Click Me!", command=click_me)
action.grid(column=1, row=0)
    
name_entered.focus()
#=================
# Start Main Loop
#=================

win.mainloop()
