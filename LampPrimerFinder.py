# -*- coding: utf-8 -*-
"""
Created on Thu Oct 25 14:07:09 2018

@author: michael
"""

import pandas as pd
import numpy as np
from Bio import SeqIO
from Bio.Seq import Seq
from Bio import SeqUtils
from Bio import Restriction as rstrn
from io import StringIO
from Bio.pairwise2 import format_alignment
from Bio import pairwise2
from time import sleep
import primer3

# ============ User has to change this ============
for record in SeqIO.parse("VMP1Fasta.txt", "fasta"):
    gene = record

# ============ User defined Varibles ============
innerLengthMax = 23
innerLengthMin = 18
innerLengthOpt = 20

innerTempMax = 61
innerTempMin = 59
innerTempOpt = 60

innerGCMax = 80
innerGCMin = 40

complementLengthMax = 25
complementLengthMin = 18
complementLengthOpt = 20

complementTempMax = 66
complementTempMin = 64
complementTempOpt = 65

complementGCMax = 60
complementGCMin = 50

outerLengthMax = 23
outerLengthMin = 18
outerLengthOpt = 20

outerTempMax = 61
outerTempMin = 59
outerTempOpt = 60

outerGCMax = 60
outerGCMin = 50

maxHairpinTm = 35

ampBuf = 40

X3Buf = 3

X3Shift = -10

interConnect = 4

numToReturn = 10

# ============ Forward Site Address ============
strtPos = 57914000

methSite = np.array([57915773, 57915665])
cutSite = np.array([57915773, 57915666])

# ============ End of User Input ============

methSiteLocal = methSite-strtPos
cutSiteLocal = cutSite-strtPos

fowardMethSite = methSiteLocal.max()
backwardMethSite = methSiteLocal.min()

fowardCutSite = cutSiteLocal.max()
backwardCutSite = cutSiteLocal.min()


print('=============================================')
print('Using restriction enzymes AsuC2I and BsaWI on VMP1 gene.')
print('=============================================')
print('Gene file sanity check:')
for i in (methSite-strtPos):
    print('Here\'s whats at position ' + str(i + strtPos) + ': ' + gene.seq[i])

print('=============================================')
print('Local Position of Forward Methylations Site: ' + str(fowardMethSite))
print('Local Position of Backward Methylations Site: ' + str(backwardMethSite))
print('=============================================')
print('DNA ROI:')
midPoint = int((fowardMethSite-backwardMethSite)/2) + backwardMethSite

sectBuf = 300

sectStart = midPoint - sectBuf
sectEnd = midPoint + sectBuf
print(str(gene.seq[sectStart:sectEnd]))


print('=============================================')
F2Start = fowardCutSite + ampBuf
F2End = F2Start + 3 * innerLengthMin

B2End = backwardCutSite - ampBuf
B2Start = B2End - 3 * innerLengthMin



# ============ Find F2 ============
primerF2 = (primer3.bindings.designPrimers(
            {
                'SEQUENCE_ID': 'F2',
                'SEQUENCE_TEMPLATE': str(gene.seq[F2Start:F2End]),
                'SEQUENCE_EXCLUDED_REGION': [0, 0] 
            },
            {
                'PRIMER_TASK': 'generic',
                'PRIMER_PICK_LEFT_PRIMER': 0,
                'PRIMER_PICK_INTERNAL_OLIGO': 0,
                'PRIMER_PICK_RIGHT_PRIMER': 1,
                'PRIMER_OPT_SIZE': innerLengthOpt,
                'PRIMER_MIN_SIZE': innerLengthMin,
                'PRIMER_MAX_SIZE': innerLengthMax,
                'PRIMER_OPT_TM': innerTempOpt,
                'PRIMER_MIN_TM': innerTempMin,
                'PRIMER_MAX_TM': innerTempMax,
                'PRIMER_MIN_GC': innerGCMin,
                'PRIMER_MAX_GC': innerGCMax,
                'PRIMER_MAX_POLY_X': 5,
                'PRIMER_SALT_MONOVALENT': 50.0,
                'PRIMER_DNA_CONC': 50.0,
                'PRIMER_MAX_NS_ACCEPTED': 0,
                'PRIMER_MAX_SELF_ANY': 12,
                'PRIMER_MAX_SELF_END': 8,
                'PRIMER_PAIR_MAX_COMPL_ANY': 12,
                'PRIMER_PAIR_MAX_COMPL_END': 8,
                'PRIMER_MAX_HAIRPIN_TH': maxHairpinTm,
                'PRIMER_NUM_RETURN': numToReturn,}))

primer = primerF2
hairpinTm = [[i,primer['PRIMER_RIGHT_' + str(i) + '_HAIRPIN_TH']] for i in range(0,primer['PRIMER_RIGHT_NUM_RETURNED'])]
hairpinTm = sorted(hairpinTm, key=lambda l:l[1])

indexNum = hairpinTm[0][0]

print('F2 primer: ' + primer['PRIMER_RIGHT_' + str(indexNum) + '_SEQUENCE'])
print('     |---> Tm = ' + '{:02.2f}'.format(primer['PRIMER_RIGHT_' + str(indexNum) + '_TM']) + 'C')
print('     |---> Hairpin Tm = ' + '{:02.2f}'.format(primer['PRIMER_RIGHT_' + str(indexNum) + '_HAIRPIN_TH']) + 'C')
print('     |---> CG% = ' + '{:02.2f}'.format(primer['PRIMER_RIGHT_' + str(indexNum) + '_GC_PERCENT']) + '%')
print('     |---> Stablity of last 5 nucleotides of 3\' end = ' + str(primer['PRIMER_RIGHT_' + str(indexNum) + '_END_STABILITY']) + ' kcal/mol')
print('     |---> Homodimer Formation Energy = ' + '{:02.2f}'.format(primer3.calcHomodimer(primer['PRIMER_RIGHT_' + str(indexNum) + '_SEQUENCE']).dg) + ' kcal/mol')


# ============ Find B2 ============
primerB2 = (primer3.bindings.designPrimers(
            {
                'SEQUENCE_ID': 'B2',
                'SEQUENCE_TEMPLATE': str(gene.seq[B2Start:B2End]),
                'SEQUENCE_EXCLUDED_REGION': [0, 0] 
            },
            {
                'PRIMER_TASK': 'generic',
                'PRIMER_PICK_LEFT_PRIMER': 1,
                'PRIMER_PICK_INTERNAL_OLIGO': 0,
                'PRIMER_PICK_RIGHT_PRIMER': 0,
                'PRIMER_OPT_SIZE': innerLengthOpt,
                'PRIMER_MIN_SIZE': innerLengthMin,
                'PRIMER_MAX_SIZE': innerLengthMax,
                'PRIMER_OPT_TM': innerTempOpt,
                'PRIMER_MIN_TM': innerTempMin,
                'PRIMER_MAX_TM': innerTempMax,
                'PRIMER_MIN_GC': innerGCMin,
                'PRIMER_MAX_GC': innerGCMax,
                'PRIMER_MAX_POLY_X': 5,
                'PRIMER_SALT_MONOVALENT': 50.0,
                'PRIMER_DNA_CONC': 50.0,
                'PRIMER_MAX_NS_ACCEPTED': 0,
                'PRIMER_MAX_SELF_ANY': 12,
                'PRIMER_MAX_SELF_END': 8,
                'PRIMER_PAIR_MAX_COMPL_ANY': 12,
                'PRIMER_PAIR_MAX_COMPL_END': 8,
                'PRIMER_MAX_HAIRPIN_TH': maxHairpinTm,
                'PRIMER_NUM_RETURN': numToReturn,}))

primer = primerB2
hairpinTm = [[i,primer['PRIMER_LEFT_' + str(i) + '_HAIRPIN_TH']] for i in range(0,primer['PRIMER_LEFT_NUM_RETURNED'])]
hairpinTm = sorted(hairpinTm, key=lambda l:l[1])

indexNum = hairpinTm[0][0]
B2Best = indexNum

print('B2 primer: ' + primer['PRIMER_LEFT_' + str(indexNum) + '_SEQUENCE'])
print('     |---> Tm = ' + '{:02.2f}'.format(primer['PRIMER_LEFT_' + str(indexNum) + '_TM']) + 'C')
print('     |---> Hairpin Tm = ' + '{:02.2f}'.format(primer['PRIMER_LEFT_' + str(indexNum) + '_HAIRPIN_TH']) + 'C')
print('     |---> CG% = ' + '{:02.2f}'.format(primer['PRIMER_LEFT_' + str(indexNum) + '_GC_PERCENT']) + '%')
print('     |---> Stablity of last 5 nucleotides of 3\' end = ' + str(primer['PRIMER_LEFT_' + str(indexNum) + '_END_STABILITY']) + ' kcal/mol')
print('     |---> Homodimer Formation Energy = ' + '{:02.2f}'.format(primer3.calcHomodimer(primer['PRIMER_LEFT_' + str(indexNum) + '_SEQUENCE']).dg) + ' kcal/mol')


F3Start = F2Start + primerF2['PRIMER_RIGHT_0'][0] + X3Buf
F3End = F3Start + 3 * outerLengthMax

B3End = B2Start + primerB2['PRIMER_LEFT_0'][0] - X3Buf
B3Start = B3End - 3 * outerLengthMax

# ============ Find F3 ============
primerF3 = (primer3.bindings.designPrimers(
            {
                'SEQUENCE_ID': 'F3',
                'SEQUENCE_TEMPLATE': str(gene.seq[F3Start:F3End]),
                'SEQUENCE_EXCLUDED_REGION': [0, 0] 
            },
            {
                'PRIMER_TASK': 'generic',
                'PRIMER_PICK_LEFT_PRIMER': 0,
                'PRIMER_PICK_INTERNAL_OLIGO': 0,
                'PRIMER_PICK_RIGHT_PRIMER': 1,
                'PRIMER_OPT_SIZE': outerLengthOpt,
                'PRIMER_MIN_SIZE': outerLengthMin,
                'PRIMER_MAX_SIZE': outerLengthMax,
                'PRIMER_OPT_TM': outerTempOpt,
                'PRIMER_MIN_TM': outerTempMin,
                'PRIMER_MAX_TM': outerTempMax,
                'PRIMER_MIN_GC': outerGCMin,
                'PRIMER_MAX_GC': outerGCMax,
                'PRIMER_MAX_POLY_X': 5,
                'PRIMER_SALT_MONOVALENT': 50.0,
                'PRIMER_DNA_CONC': 50.0,
                'PRIMER_MAX_NS_ACCEPTED': 0,
                'PRIMER_MAX_SELF_ANY': 12,
                'PRIMER_MAX_SELF_END': 8,
                'PRIMER_PAIR_MAX_COMPL_ANY': 12,
                'PRIMER_PAIR_MAX_COMPL_END': 8,
                'PRIMER_MAX_HAIRPIN_TH': maxHairpinTm,
                'PRIMER_NUM_RETURN': numToReturn,}))

primer = primerF3
hairpinTm = [[i,primer['PRIMER_RIGHT_' + str(i) + '_HAIRPIN_TH']] for i in range(0,primer['PRIMER_RIGHT_NUM_RETURNED'])]
hairpinTm = sorted(hairpinTm, key=lambda l:l[1])

indexNum = hairpinTm[0][0]
F3Best = indexNum

print('F3 primer: ' + primer['PRIMER_RIGHT_' + str(indexNum) + '_SEQUENCE'])
print('     |---> Tm = ' + '{:02.2f}'.format(primer['PRIMER_RIGHT_' + str(indexNum) + '_TM']) + 'C')
print('     |---> Hairpin Tm = ' + '{:02.2f}'.format(primer['PRIMER_RIGHT_' + str(indexNum) + '_HAIRPIN_TH']) + 'C')
print('     |---> CG% = ' + '{:02.2f}'.format(primer['PRIMER_RIGHT_' + str(indexNum) + '_GC_PERCENT']) + '%')
print('     |---> Stablity of last 5 nucleotides of 3\' end = ' + str(primer['PRIMER_RIGHT_' + str(indexNum) + '_END_STABILITY']) + ' kcal/mol')
print('     |---> Homodimer Formation Energy = ' + '{:02.2f}'.format(primer3.calcHomodimer(primer['PRIMER_RIGHT_' + str(indexNum) + '_SEQUENCE']).dg) + ' kcal/mol')


# ============ Find B3 ============
primerB3 = (primer3.bindings.designPrimers(
            {
                'SEQUENCE_ID': 'B3',
                'SEQUENCE_TEMPLATE': str(gene.seq[B3Start:B3End]),
                'SEQUENCE_EXCLUDED_REGION': [0, 0] 
            },
            {
                'PRIMER_TASK': 'generic',
                'PRIMER_PICK_LEFT_PRIMER': 1,
                'PRIMER_PICK_INTERNAL_OLIGO': 0,
                'PRIMER_PICK_RIGHT_PRIMER': 0,
                'PRIMER_OPT_SIZE': outerLengthOpt,
                'PRIMER_MIN_SIZE': outerLengthMin,
                'PRIMER_MAX_SIZE': outerLengthMax,
                'PRIMER_OPT_TM': outerTempOpt,
                'PRIMER_MIN_TM': outerTempMin,
                'PRIMER_MAX_TM': outerTempMax,
                'PRIMER_MIN_GC': outerGCMin,
                'PRIMER_MAX_GC': outerGCMax,
                'PRIMER_MAX_POLY_X': 5,
                'PRIMER_SALT_MONOVALENT': 50.0,
                'PRIMER_DNA_CONC': 50.0,
                'PRIMER_MAX_NS_ACCEPTED': 0,
                'PRIMER_MAX_SELF_ANY': 12,
                'PRIMER_MAX_SELF_END': 8,
                'PRIMER_PAIR_MAX_COMPL_ANY': 12,
                'PRIMER_PAIR_MAX_COMPL_END': 8,
                'PRIMER_MAX_HAIRPIN_TH': maxHairpinTm,
                'PRIMER_NUM_RETURN': numToReturn,}))

primer = primerB3
hairpinTm = [[i,primer['PRIMER_LEFT_' + str(i) + '_HAIRPIN_TH']] for i in range(0,primer['PRIMER_LEFT_NUM_RETURNED'])]
hairpinTm = sorted(hairpinTm, key=lambda l:l[1])

indexNum = hairpinTm[0][0]
B3Best = indexNum

print('B3 primer: ' + primer['PRIMER_LEFT_' + str(indexNum) + '_SEQUENCE'])
print('     |---> Tm = ' + '{:02.2f}'.format(primer['PRIMER_LEFT_' + str(indexNum) + '_TM']) + 'C')
print('     |---> Hairpin Tm = ' + '{:02.2f}'.format(primer['PRIMER_LEFT_' + str(indexNum) + '_HAIRPIN_TH']) + 'C')
print('     |---> CG% = ' + '{:02.2f}'.format(primer['PRIMER_LEFT_' + str(indexNum) + '_GC_PERCENT']) + '%')
print('     |---> Stablity of last 5 nucleotides of 3\' end = ' + str(primer['PRIMER_LEFT_' + str(indexNum) + '_END_STABILITY']) + ' kcal/mol')
print('     |---> Homodimer Formation Energy = ' + '{:02.2f}'.format(primer3.calcHomodimer(primer['PRIMER_LEFT_' + str(indexNum) + '_SEQUENCE']).dg) + ' kcal/mol')


F1cEnd = F2Start + primerF2['PRIMER_RIGHT_0'][0] - primerF2['PRIMER_RIGHT_0'][1]
#F1cStart = F1cEnd - complementLengthMax

B1cStart = B2Start + primerB2['PRIMER_LEFT_0'][0] + primerB2['PRIMER_LEFT_0'][1]
#B1cEnd = B1cStart + complementLengthMax

B1cEnd = int(B1cStart + (F1cEnd - B1cStart)/2)
F1cStart = B1cEnd + 1

# ============ Find F1c ============
primerF1c = (primer3.bindings.designPrimers(
            {
                'SEQUENCE_ID': 'F1c',
                'SEQUENCE_TEMPLATE': str(gene.seq[F1cStart:F1cEnd]),
                'SEQUENCE_EXCLUDED_REGION': [0, 0] 
            },
            {
                'PRIMER_TASK': 'generic',
                'PRIMER_PICK_LEFT_PRIMER': 1,
                'PRIMER_PICK_INTERNAL_OLIGO': 0,
                'PRIMER_PICK_RIGHT_PRIMER': 0,
                'PRIMER_OPT_SIZE': complementLengthOpt,
                'PRIMER_MIN_SIZE': complementLengthMin,
                'PRIMER_MAX_SIZE': complementLengthMax,
                'PRIMER_OPT_TM': complementTempOpt,
                'PRIMER_MIN_TM': complementTempMin,
                'PRIMER_MAX_TM': complementTempMax,
                'PRIMER_MIN_GC': complementGCMin,
                'PRIMER_MAX_GC': complementGCMax,
                'PRIMER_MAX_POLY_X': 5,
                'PRIMER_SALT_MONOVALENT': 50.0,
                'PRIMER_DNA_CONC': 50.0,
                'PRIMER_MAX_NS_ACCEPTED': 0,
                'PRIMER_MAX_SELF_ANY': 12,
                'PRIMER_MAX_SELF_END': 8,
                'PRIMER_PAIR_MAX_COMPL_ANY': 12,
                'PRIMER_PAIR_MAX_COMPL_END': 8,
                'PRIMER_MAX_HAIRPIN_TH': maxHairpinTm,
                'PRIMER_NUM_RETURN': numToReturn,}))

primer = primerF1c
hairpinTm = [[i,primer['PRIMER_LEFT_' + str(i) + '_HAIRPIN_TH']] for i in range(0,primer['PRIMER_LEFT_NUM_RETURNED'])]
hairpinTm = sorted(hairpinTm, key=lambda l:l[1])

indexNum = hairpinTm[0][0]
F1cBest = indexNum

print('F1c primer: ' + primer['PRIMER_LEFT_' + str(indexNum) + '_SEQUENCE'])
print('     |---> Tm = ' + '{:02.2f}'.format(primer['PRIMER_LEFT_' + str(indexNum) + '_TM']) + 'C')
print('     |---> Hairpin Tm = ' + '{:02.2f}'.format(primer['PRIMER_LEFT_' + str(indexNum) + '_HAIRPIN_TH']) + 'C')
print('     |---> CG% = ' + '{:02.2f}'.format(primer['PRIMER_LEFT_' + str(indexNum) + '_GC_PERCENT']) + '%')
print('     |---> Stablity of last 5 nucleotides of 3\' end = ' + str(primer['PRIMER_LEFT_' + str(indexNum) + '_END_STABILITY']) + ' kcal/mol')
print('     |---> Homodimer Formation Energy = ' + '{:02.2f}'.format(primer3.calcHomodimer(primer['PRIMER_LEFT_' + str(indexNum) + '_SEQUENCE']).dg) + ' kcal/mol')



# ============ Find B1c ============
primerB1c = (primer3.bindings.designPrimers(
            {
                'SEQUENCE_ID': 'B1c',
                'SEQUENCE_TEMPLATE': str(gene.seq[B1cStart:B1cEnd]),
                'SEQUENCE_EXCLUDED_REGION': [0, 0] 
            },
            {
                'PRIMER_TASK': 'generic',
                'PRIMER_PICK_LEFT_PRIMER': 0,
                'PRIMER_PICK_INTERNAL_OLIGO': 0,
                'PRIMER_PICK_RIGHT_PRIMER': 1,
                'PRIMER_OPT_SIZE': complementLengthOpt,
                'PRIMER_MIN_SIZE': complementLengthMin,
                'PRIMER_MAX_SIZE': complementLengthMax,
                'PRIMER_OPT_TM': complementTempOpt,
                'PRIMER_MIN_TM': complementTempMin,
                'PRIMER_MAX_TM': complementTempMax,
                'PRIMER_MIN_GC': complementGCMin,
                'PRIMER_MAX_GC': complementGCMax,
                'PRIMER_MAX_POLY_X': 5,
                'PRIMER_SALT_MONOVALENT': 50.0,
                'PRIMER_DNA_CONC': 50.0,
                'PRIMER_MAX_NS_ACCEPTED': 0,
                'PRIMER_MAX_SELF_ANY': 12,
                'PRIMER_MAX_SELF_END': 8,
                'PRIMER_PAIR_MAX_COMPL_ANY': 12,
                'PRIMER_PAIR_MAX_COMPL_END': 8,
                'PRIMER_MAX_HAIRPIN_TH': maxHairpinTm,
                'PRIMER_NUM_RETURN': numToReturn,}))

primer = primerB1c
hairpinTm = [[i,primer['PRIMER_RIGHT_' + str(i) + '_HAIRPIN_TH']] for i in range(0,primer['PRIMER_RIGHT_NUM_RETURNED'])]
hairpinTm = sorted(hairpinTm, key=lambda l:l[1])

indexNum = hairpinTm[0][0]
B1cBest = indexNum

print('B1c primer: ' + primer['PRIMER_RIGHT_' + str(indexNum) + '_SEQUENCE'])
print('     |---> Tm = ' + '{:02.2f}'.format(primer['PRIMER_RIGHT_' + str(indexNum) + '_TM']) + 'C')
print('     |---> Hairpin Tm = ' + '{:02.2f}'.format(primer['PRIMER_RIGHT_' + str(indexNum) + '_HAIRPIN_TH']) + 'C')
print('     |---> CG% = ' + '{:02.2f}'.format(primer['PRIMER_RIGHT_' + str(indexNum) + '_GC_PERCENT']) + '%')
print('     |---> Stablity of last 5 nucleotides of 3\' end = ' + str(primer['PRIMER_RIGHT_' + str(indexNum) + '_END_STABILITY']) + ' kcal/mol')
print('     |---> Homodimer Formation Energy = ' + '{:02.2f}'.format(primer3.calcHomodimer(primer['PRIMER_RIGHT_' + str(indexNum) + '_SEQUENCE']).dg) + ' kcal/mol')


print('=============================================')
print('F3 primer: ' + primerF3['PRIMER_RIGHT_' + str(F3Best) + '_SEQUENCE'])
print('B3 primer: ' + primerB3['PRIMER_LEFT_' + str(B3Best) + '_SEQUENCE'])

FIP = Seq(primerF1c['PRIMER_LEFT_' + str(F1cBest) + '_SEQUENCE']) + Seq(''.join([ 'T' for i in range(0,interConnect)])) + Seq(primerF2['PRIMER_RIGHT_' + str(F2Best) + '_SEQUENCE'])

BIP = Seq(primerB1c['PRIMER_RIGHT_' + str(B1cBest) + '_SEQUENCE']) + Seq(''.join([ 'T' for i in range(0,interConnect)])) + Seq(primerB2['PRIMER_LEFT_' + str(B2Best) + '_SEQUENCE'])



print('FIB primer: ' + str(FIP))
print('BIB primer: ' + str(BIP))

print('=============================================')
print('Homodimer Formation Energy Analysis for FIP/BIP:')
print('FIP: ' + '{:02.2f}'.format(primer3.calcHomodimer(str(FIP)).dg) + ' kcal/mol')
print('BIP: ' + '{:02.2f}'.format(primer3.calcHomodimer(str(BIP)).dg) + ' kcal/mol')
print('=============================================')
print('Heterodimer Formation Energy Analysis:')
print('FIP-BIP: ' + '{:02.2f}'.format(primer3.calcHeterodimer(str(FIP), str(BIP)).dg) + ' kcal/mol')
print('FIP-F3: ' + '{:02.2f}'.format(primer3.calcHeterodimer(str(FIP), primerF3['PRIMER_RIGHT_' + str(F3Best) + '_SEQUENCE']).dg ) + ' kcal/mol')
print('FIP-B3: ' + '{:02.2f}'.format(primer3.calcHeterodimer(str(FIP), primerB3['PRIMER_LEFT_' + str(B3Best) + '_SEQUENCE']).dg ) + ' kcal/mol')
print('BIP-F3: ' + '{:02.2f}'.format(primer3.calcHeterodimer(str(BIP), primerF3['PRIMER_RIGHT_' + str(F3Best) + '_SEQUENCE']).dg ) + ' kcal/mol')
print('BIP-B3: ' + '{:02.2f}'.format(primer3.calcHeterodimer(str(BIP), primerB3['PRIMER_LEFT_' + str(B3Best) + '_SEQUENCE']).dg ) + ' kcal/mol')
print('F3-B3: ' + '{:02.2f}'.format(primer3.calcHeterodimer(primerF3['PRIMER_RIGHT_' + str(F3Best) + '_SEQUENCE'], primerB3['PRIMER_LEFT_' + str(B3Best) + '_SEQUENCE']).dg ) + ' kcal/mol')
print('=============================================')






























