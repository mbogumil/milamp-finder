# -*- coding: utf-8 -*-
"""
Created on Sun Oct 21 02:33:51 2018

@author: michael
"""

import pandas as pd
import numpy as np
from Bio import SeqIO
from Bio import SeqUtils
from Bio import Restriction as rstrn
from io import StringIO
from Bio.pairwise2 import format_alignment
from Bio import pairwise2
from time import sleep

# ============ User has to change this ============
for record in SeqIO.parse("BRCA1Thomas.txt", "fasta"):
    gene = record

# ============ User has to change this ============
strtPos = 43124000 #BRCA1Thomas

# ============ User has to change this ============
methSite = np.array([43125347, 43125364, 43125372, 43125377, 43125383, 43125409, 43125411, 43125419]) #BRCA1Thomas


#methSiteAdj = np.concatenate([methSite, (methSite + 1), (methSite - 1), (methSite + 2), (methSite - 2)])

# =============================================================================
# print(gene.seq[1350:1450])
# print(SeqUtils.GC(gene.seq))
# =============================================================================

noCpGFlag = False

print('Gene file sanity test:')
for i in (methSite-strtPos):
    print('Is there a CpG at position ' + str(i + strtPos) + '?... ', end="")
    if (gene.seq[i] == 'C') and (gene.seq[i+1] == 'G'):
        print('Yes!')
    else:
        print('No!')
        noCpGFlag = True

if (noCpGFlag == True):
    print('Some of the methylation sites entered do not contain CpGs. The methylation sites should be checked.')
        
    

print('=============================================')

rb_supp = rstrn.RestrictionBatch(first=[], suppliers=['C','B','E','I','K','J','M','O','N','Q','S','R','V','Y','X'])

ReEnzymeSites = rb_supp.search(gene.seq)

for key, value in ReEnzymeSites.items() :
    match = set(np.array([np.arange(2*len(key)-1)-(len(key) - 1) + i for i in (methSite - strtPos)]).flatten()) & set(value)
    if (len(match) > 0):
        print(str(key) + ' matches at ', end="")
        for i in (np.array(list(match)) + strtPos):
            print(str(i) + ' ', end="")
            
        print('')
        
        print(str(key) + ' is ' + str(len(key)) + ' and also cuts at ', end="")
        for i in (np.array(value) + strtPos):
            print(str(i) + ' ', end="")
            
        print('')
        print('=============================================')


        # [np.arange(2*len(key)-1)-(len(key) - 1) + i for i in methSite]
            