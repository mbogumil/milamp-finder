# -*- coding: utf-8 -*-
"""
Created on Sat Nov 10 23:58:55 2018

@author: michael
"""

import math
import numpy as np
import random as rnd



def funct(data):
    return np.cos(data[0])*np.cos(data[1])


inputMat = (2*np.random.rand(2, 10000) - 1) * math.pi/2

out = funct(inputMat)

#full = np.array([out, inputMat])
full = np.c_[(out, inputMat.T)]


full = full[full[:,0].argsort()]